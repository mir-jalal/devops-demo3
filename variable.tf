variable "region" {
  type = string
  default = "us-east-1"
}

variable "az_first" {
  type = string
  default = "us-east-1a"
}

variable "az_second" {
  type = string
  default = "us-east-1b"
}

variable "ami_name" {
  type = string
}

variable "ami_owner" {
  type = string
}

variable "aws_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "db_instance_class" {
  type    = string
  default = "db.t2.small"
}

variable "db_name"{
  type    = string
  default = "petclinicdb"
}

variable "db_storage_type" {
  type    = string
  default = "gp2"
}

variable "db_allocated_storage"{
  type    = number
  default = 20
}

variable "db_max_allocated_storage"{
  type    = number
  default = 21
}

variable "devops_ssh_key" {
  type = string
}

variable "db_password" {
  type      = string
  sensitive = true
}

variable "db_username" {
  type = string
}
