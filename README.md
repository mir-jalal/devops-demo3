# DEVOPS-DEMO-3
___

This is Demo-3 task repo for IbaTech Academy
___

## Part 1 - Create Docker Image and Push it docker registry

To make the project clear and keep the source code separate from infrastructure, 
I have created the second [repo](https://gitlab.com/mir.jalal/spring-petclinic). 
I created pipeline so when source code is updated, the image at registry will be recreated.

* At this part of ```.gitlab-ci.yml``` I use ```MAVEN_OPTS``` variable, so it will store ```.m2/repository```
in the working directory. So I can cache it.
  <pre>
  variables:
      MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  
  cache:
    paths:
      - .m2/repository
  </pre>

* Pipeline has 3 stages and each stage has one job. The first job builds the project with ```mvn package``` command.
The second does unit tests and integration tests with ```mvn verify``` command. If everything is okay, the third job builds docker image and pushes it into docker registry
  
  > You can look at the full ```.gitlab-ci.yml``` file by [this link](https://gitlab.com/mir.jalal/spring-petclinic/-/blob/master/.gitlab-ci.yml) 

## Part 2 - Packer
As I will later use docker images in my EC2 instances I need some packages and configurations pre-installed in AMI.
I used packer for it and created custom AMI for later usage.
  > To run packer build you need to specify aws credentials. There are several ways to do it, however I used ```.aws/credentials``` file for it.
  > To get more information use [this link](https://www.packer.io/docs/builders/amazon#environment-variables)
  
## Part 3 - Terraform

* Several ```.tf``` files manage VPC network, EC2 instance based on AMI created with packer, and RDS. 
You should specify some variables beforehand to run ```terraform apply```:
  - ```TF_VAR_devops_ssh_key``` - ```.pem``` file for creating ssh connection
  - ```TF_VAR_db_password``` - Password that will be used to connect RDS
  - ```TF_VAR_db_username``` - Username for RDS
  
* In the ```network.tf``` I have created following resources:
  - ```aws_vpc``` - provides a Virtual Private Network
  - ```aws_subnet``` - I created 1 public (for EC2 instance) and 2 private (one reserved as Database Subnet Group needs at least 2 subnets)
  - ```aws_security_group``` - One for EC2 instance and another for RDS
  - ```aws_internet_gateway``` - To make EC2 instance have internet access 
  - ```aws_route_table``` - With the help of this resource the packets will be routed to internet gateway resource.
  - ```aws_route_table_association``` - It shows packets in which subnets will be routed
  
* In the ```main.tf``` I created ```aws_instance``` based on ami created with packer.
  > It is mentioned that _Provisioners should only be used as a last resort. For most common situations there are better alternatives._
  > That is why, I provisioned EC2 instance with Ansible.
  >
  > To get more information: [remote-exec Provisioners](https://www.terraform.io/docs/language/resources/provisioners/remote-exec.html)
  > 

* With the ```database.tf``` RDS is created.
* After all terraform exports two attributes:
  - ```db_address``` -  The hostname of the RDS instance
  - ```vm_address``` -  The public IP address assigned to the instance
  
  > I will use these two attributes for provisioning EC2 instance and test the health-check
  
## Part 4 - Gitlab CI
I have created 7 stages and one job for each stage:
* ```init``` - It initializes terraform and installs required plugins 
* ```validate``` - It validates if the all ```.tf``` files are valid
* ```plan``` - It creates execution plan
* ```apply``` - It executes the actions proposed in a Terraform plan
* ```provision``` - This job uses ansible-playbook to provision EC2 instance
* ```health-check``` - It checks if the application is up and running
* ```destroy``` - It destroys all remote objects managed by a particular Terraform configuration

  > As the [ansible images](https://hub.docker.com/r/ansible/ansible) are very old, I preferred to use my own docker image for provisioning.
  > You can use that image by ```docker pull registry.gitlab.com/mir.jalal/ansible:latest```
  > 

___
### To-Do-List

- Subtask I  - Create [Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) scripts for AWS infrastructure:
   * [x] Create AWS VPC.
   * [x] Configure access and network groups.
   * [x] Create 2 EC2 instances(use t3.micro types).
   * [x] Provision 1st EC2 instance install MySQL and install all dependencies. (Or create [AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html) based on that ec2 instance, and use it as image.)
   * [x] Install all dependencies on 2nd EC2 instance. (Or create AMI based on that ec2 instance, and use it as image.)
   * [x] Deploy jar application and run it on 2nd instance.

- Subtask II  - Gitlab
    * [x] Setup Gitlab account/Gitlab.
    * [x] Create a job that will be triggered on changes in master branch in gitlab repo. It should build your project and create jar package.
    * [x] Create a 2nd Job and use your Terraform scripts for infrastructure deployment.
    * [x] Create a step to check if the application is up and running(health check).
  
- Additional tasks
    * [ ] Create the same application infrastructure using GCP cloud provider.
    * [x] Use docker images and deploy them to EC2 instances instead of bare instances.
    * [x] Use MySQL database in AWS RDS instead ec2 instance.