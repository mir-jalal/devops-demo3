output "db_address" {
  value = aws_db_instance.petclinic_db.address
}

output "vm_address" {
  value = aws_instance.petclinic_vm.public_ip
}
