resource "aws_db_instance" "petclinic_db" {
  allocated_storage      = var.db_allocated_storage
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = var.db_instance_class
  name                   = var.db_name
  username               = var.db_username
  password               = var.db_password
  storage_type           = var.db_storage_type
  publicly_accessible    = false
  multi_az               = false
  max_allocated_storage  = var.db_max_allocated_storage
  skip_final_snapshot    = true
  identifier             = var.db_name
  db_subnet_group_name   = aws_db_subnet_group.petclinic_db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.petclinic_db_security_group.id]
}
