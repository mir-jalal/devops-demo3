terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_ami" "petclinic_ami" {
  most_recent = true

  filter {
    name 	= "name"
    values 	= [var.ami_name]
  }

  filter {
    name	= "virtualization-type"
    values 	= ["hvm"]
  }
 
  owners = [var.ami_owner]
}

resource "aws_instance" "petclinic_vm" {
  ami		    = data.aws_ami.petclinic_ami.id
  instance_type	= var.aws_instance_type
  key_name      = "ssh-key"

  subnet_id       = aws_subnet.petclinic_public_subnet.id
  security_groups = [aws_security_group.petclinic_security_group.id]

  tags = {
    Name = "PetclinicApp"
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name    = "ssh-key"
  public_key  = var.devops_ssh_key
}

