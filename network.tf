resource "aws_vpc" "petclinic_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "petclinic"
  }
}

resource "aws_subnet" "petclinic_public_subnet" {
  cidr_block = "10.0.10.0/24"
  vpc_id = aws_vpc.petclinic_vpc.id
  availability_zone = var.az_first
  map_public_ip_on_launch = true

  tags = {
    Name = "petclinic_public_subnet"
  }
}

resource "aws_subnet" "petclinic_private_subnet"{
  cidr_block = "10.0.20.0/24"
  vpc_id = aws_vpc.petclinic_vpc.id
  availability_zone = var.az_first

  tags = {
    Name = "petclinic_private_subnet"
  }
}

resource "aws_subnet" "petclinic_private_subnet_reserved"{
  cidr_block = "10.0.30.0/24"
  vpc_id = aws_vpc.petclinic_vpc.id
  availability_zone = var.az_second

  tags = {
    Name = "petclinic_private_subnet"
  }
}

resource "aws_db_subnet_group" "petclinic_db_subnet_group" {
  name = "petclinic_db_subnet_group"
  subnet_ids = [aws_subnet.petclinic_private_subnet.id, aws_subnet.petclinic_private_subnet_reserved.id]

  tags = {
    Name = "petclinic_db_subnet_group"
  }
}

resource "aws_security_group" "petclinic_security_group" {
  vpc_id = aws_vpc.petclinic_vpc.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH for VPC"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "petclinic_db_security_group" {
  vpc_id = aws_vpc.petclinic_vpc.id

  ingress {
    description = "SSH for VPC"
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    cidr_blocks = [aws_vpc.petclinic_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_internet_gateway" "petclinic_internet_gateway" {
  vpc_id = aws_vpc.petclinic_vpc.id
}

resource "aws_route_table" "petclinic_route_table" {
  vpc_id = aws_vpc.petclinic_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.petclinic_internet_gateway.id
  }
}

resource "aws_route_table_association" "petclinic_route_table_association" {
  route_table_id = aws_route_table.petclinic_route_table.id
  subnet_id = aws_subnet.petclinic_public_subnet.id
}
